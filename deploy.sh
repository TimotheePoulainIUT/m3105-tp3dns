#!/bin/bash
cd /root/m3105-tp3dns

#Virtual servers
for i in dwikiorg diutre drtiutre dorg dre aRootServer
do
  echo "Current machine : $i"
  #Host
  echo "Moving into folder $i"
  cd ./$i

  #Virtual machine
  #Import files
  echo "Importing files to server $i"
  himage $i mkdir /etc/named
  hcp * $i:/etc/named/
  hcp named.conf $i:/etc/

  #Delete the /etc/named/named.conf
  himage $i cd /etc/named
  himage $i rm /etc/named/named.conf

  #Launch named service
  echo "Starting NAMED service on $i"
  himage $i named -c /etc/named.conf


  #The end
  cd ..
  echo "$i configuration is finished."
done
echo "Servers configuration finished."

#########################################################
#Virtual pcs
for i in pc1 pc2
do
  echo "Current machine : $i"
  #Host
  echo "Moving into folder $i"
  cd ./$i

  #Virtual machine
  #Delete the old and import the new resolv.conf
  echo "Importing resolv.conf to computer $i"
  himage $i rm /etc/resolv.conf
  hcp resolv.conf $i:/etc/

  #The end
  cd ..
  echo "$i configuration is finished."
done
echo "Computers configuration finished."
echo "Everything is done."
